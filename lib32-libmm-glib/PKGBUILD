# $Id: PKGBUILD 205833 2017-01-06 21:47:02Z alucryd $
# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): Ionut Biru <ibiru@archlinux.org>
# Contributor (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=lib32-libmm-glib
pkgver=1.6.4
pkgrel=1.hyperbola1
pkgdesc='ModemManager library, with lib32-eudev support'
arch=('x86_64')
url='https://www.freedesktop.org/wiki/Software/ModemManager/'
license=('GPL2' 'LGPL2.1')
depends=('lib32-glib2' 'libmm-glib')
makedepends=('gcc-multilib' 'intltool' 'lib32-libmbim' 'lib32-libqmi'
             'lib32-polkit' 'lib32-eudev' 'python' 'vala')
source=("https://www.freedesktop.org/software/ModemManager/ModemManager-${pkgver}.tar.xz")
sha512sums=('6b31ce186adce445cec8964df751b6146a86271e6c14d860740ae66cfe296ac2ac4df21079357775ac5f7a5837c80a7f8db21a2680bc6b45802f9928565f1c73')

build() {
  cd ModemManager-${pkgver}

  export CC='gcc -m32'
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

  ./configure \
    --prefix='/usr' \
    --libdir='/usr/lib32' \
    --localstatedir='/var' \
    --sbindir='/usr/bin' \
    --sysconfdir='/etc' \
    --disable-gtk-doc-html \
    --disable-static \
    --with-dbus-sys-dir='/usr/share/dbus-1/system.d' \
    --with-polkit='permissive' \
    --with-suspend-resume='no' \
    --with-udev-base-dir='/usr/lib32/udev'
  sed -i -e 's/ -shared / -Wl,-O1,--as-needed\0/g' libtool
  make -C libmm-glib
  make -C data
}

package() {
  cd ModemManager-${pkgver}

  make DESTDIR="${pkgdir}" -C libmm-glib install
  make DESTDIR="${pkgdir}" -C data install
  rm -rf "${pkgdir}"/{etc,usr/{include,lib,share}}
}

# vim: ts=2 sw=2 et:
