# Maintainer (Arch): Sven-Hendrik Haase <sh@lutzhaase.com>
# Contributor (Arch): Maximilian Stein <maxarchpkgbuild@hmamail.com>
# Contributor (Arch): josephgbr <rafael.f.f1@gmail.com>
# Contributor (Arch): Taylor Lookabaugh <jesus.christ.i.love@gmail.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

_pkgbase=libnl
pkgname=lib32-${_pkgbase}
pkgver=3.2.29
_debver=3.2.27
_debrel=2
pkgrel=1.hyperbola1
pkgdesc="Library for applications dealing with netlink sockets (32-bit)"
arch=('x86_64')
url="https://www.infradead.org/~tgr/libnl/"
license=('LGPL-2.1')
depends=('lib32-glibc' "${_pkgbase}")
makedepends=('gcc-multilib' 'quilt')
options=('!libtool')
source=(https://github.com/thom311/libnl/releases/download/libnl${pkgver//./_}/libnl-${pkgver}.tar.gz{,.sig}
        https://deb.debian.org/debian/pool/main/libn/libnl3/libnl3_$_debver-$_debrel.debian.tar.xz
        fs52778.patch)
sha512sums=('45e22b02368c479e01db7160345a0d3c73abf345e7b49bf350811243a17f307f5c6c3df639ec1e0c456707578aca8d69aad8afa697ac21449f521a3e39712670'
            'SKIP'
            'f19aa9da3ab77a25b73e0238c1008fd928f6d655e0df5511923a3b3ff59095914cc40150d48b41821d90137adbfff24b84c54ce59e1633cd350a12ccd3b24895'
            '68e84f40e23ed61f52c841bbc38dca867a9b144fcf8b1ac4349bb1155b3abdd4915ce7f5f0854ad18e2de08a4ba7f3b55b50f309e8f1419b9d71adad99c2074c')
validpgpkeys=('49EA7C670E0850E7419514F629C2366E4DFC5728') # Thomas Haller

prepare() {
  cd ${_pkgbase}-${pkgver}
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/debian/etc-libnl-3.diff || true
    rm -v debian/patches/debian/no-symvers.diff || true

    quilt push -av
  fi
  patch -p1 -i "${srcdir}"/fs52778.patch
}

build() {
  export CC='gcc -m32'
  export CXX='g++ -m32'
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

  cd ${_pkgbase}-${pkgver}
  ./configure \
    --prefix=/usr \
    --sysconfdir=/etc \
    --disable-static \
    --libdir=/usr/lib32
  make
}

check() {
  cd ${_pkgbase}-${pkgver}
  make check
}

package() {
  cd ${_pkgbase}-${pkgver}
  make DESTDIR="${pkgdir}" install
  install -dm755 $pkgdir/lib32
  mv "${pkgdir}"/usr/lib32/libnl{,-genl}-3.so* "${pkgdir}"/lib32
  rm -rf "${pkgdir}"/{etc,usr/{bin,include,share}}
  install -Dm644 COPYING "${pkgdir}"/usr/share/licenses/${pkgname}/COPYING
}
