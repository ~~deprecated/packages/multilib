# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgbasename=libxdmcp
pkgname=lib32-$_pkgbasename
_openbsdver=6.5
pkgver=1.1.2
pkgrel=1.hyperbola1
pkgdesc="X11 Display Manager Control Protocol library (32-bit), provided by Xenocara"
arch=(x86_64)
url="https://www.xenocara.org"
license=('X11')
depends=('lib32-glibc' "$_pkgbasename")
makedepends=('xenocara-util-macros' 'gcc-multilib')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxdmcp-$pkgver.tar.xz{,.sig})
sha512sums=('48ee6ba7445d66b54c8edf593d3c559aa26584b32d9b9ed67148d50305d8840f266bceb062f4d6eaf29670829c6f7f7fb3c05446fb27b7af558efb5a5e080bc9'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libXdmcp"
  autoreconf -vfi
}

build() {
  export CC="gcc -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  cd "xenocara-$_openbsdver/lib/libXdmcp"
  ./configure --prefix=/usr --sysconfdir=/etc --disable-static \
              --libdir=/usr/lib32
  make
}

package() {
  cd "xenocara-$_openbsdver/lib/libXdmcp"
  make DESTDIR="$pkgdir" install

  rm -rf $pkgdir/usr/{include,share}
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
