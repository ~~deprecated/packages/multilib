# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Artix): Eric Bélanger <eric@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>

_pkgname=gpm
pkgname=lib32-$_pkgname
pkgver=1.20.7
_debver=1.20.7
_debrel=5
pkgrel=1.hyperbola2
pkgdesc="A mouse server for the console and xterm (32-bit)"
arch=('i686' 'x86_64')
url="https://www.nico.schottelius.org/software/gpm/"
license=('GPL-2')
depends=('lib32-ncurses' "$_pkgname")
makedepends=('quilt')
options=('!makeflags')
source=(https://www.nico.schottelius.org/software/gpm/archives/${_pkgname}-${pkgver}.tar.lzma
        https://deb.debian.org/debian/pool/main/g/gpm/gpm_$_debver-$_debrel.debian.tar.xz)
sha512sums=('a502741e2f457b47e41c6d155b1f7ef7c95384fd394503f82ddacf80cde9cdc286c906c77be12b6af8565ef1c3ab24d226379c1dcebcfcd15d64bcf3e94b63b9'
            'cb92b391d5883d7967941cd3d83bf5e8936143dd73dc720e2ef98a89308352499248ad0a5400d5f328299de7c47777a8a15fb58c84e3d1d72e108ccc94707d25')

prepare() {
  cd "${srcdir}/${_pkgname}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/007_doc_fix_000 || true

    quilt push -av
  fi
}

build() {
  export CC='gcc -m32'
  export CXX='g++ -m32'
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

  cd "${srcdir}/${_pkgname}-${pkgver}"
  ./autogen.sh
  ./configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib32
  make
}

package() {
  cd "${srcdir}/${_pkgname}-${pkgver}"
  make DESTDIR="${pkgdir}" install

  # fix library
  ln -s libgpm.so.2 "${pkgdir}/usr/lib32/libgpm.so"
  chmod 755 "${pkgdir}"/usr/lib32/libgpm.so.2*

  rm -rf "${pkgdir}"/usr/{{s,}bin,include,share} "$pkgdir"/etc

  # install license
  install -D -m644 COPYING "${pkgdir}/usr/share/licenses/${pkgname}/COPYING"
}
