# Maintainer (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgbasename=libxrender
pkgname=lib32-$_pkgbasename
_openbsdver=6.5
pkgver=0.9.10
pkgrel=1.hyperbola1
pkgdesc="X Rendering Extension client library (32-bit), provided by Xenocara"
arch=('x86_64')
url="https://www.xenocara.org"
license=('Expat')
depends=('lib32-libx11>=1.3.4' "$_pkgbasename")
makedepends=('pkg-config' 'gcc-multilib' 'xenocara-proto' 'xenocara-util-macros')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxrender-$pkgver.tar.xz{,.sig})
sha512sums=('d9563d5dc7687af55282509f96315370f3930ad0f76af8f8d411a03589e78b4efb9937bd2852bba2c48546f21ecf6d5b68d8b03694ad5b7c2bcbff65e793c7ed'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libXrender"
  autoreconf -vfi
}

build() {
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  cd "xenocara-$_openbsdver/lib/libXrender"
  ./configure --prefix=/usr --disable-static --libdir=/usr/lib32
  make
}

package() {
  cd "xenocara-$_openbsdver/lib/libXrender"
  make DESTDIR="$pkgdir" install

  rm -rf $pkgdir/usr/{include,share,bin}
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
