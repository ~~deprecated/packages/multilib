# $Id: PKGBUILD 215726 2017-03-11 20:43:17Z alucryd $
# Maintainer (Arch): Maxime Gauduin <alucryd@archlinux.org>
# Contributor (Arch): FadeMind <fademind@gmail.com>
# Contributor (Arch): Evangelos Foutras <evangelos@foutrelis.com>
# Maintainer: André Silva <emulatorman@hyperbola.info>

pkgname=lib32-libgudev
pkgver=231
_debver=231
_debrel=2
pkgrel=1.hyperbola1
pkgdesc='GObject bindings for libudev, with lib32-eudev support'
arch=('x86_64')
url='https://wiki.gnome.org/Projects/libgudev'
license=('LGPL2.1')
depends=('lib32-glib2' 'lib32-eudev' 'libgudev')
makedepends=('gcc-multilib' 'quilt')
source=("https://download.gnome.org/sources/libgudev/${pkgver}/libgudev-${pkgver}.tar.xz"
        "https://deb.debian.org/debian/pool/main/libg/libgudev/libgudev_$_debver-$_debrel.debian.tar.xz")
sha512sums=('3989025168c9e7e5e3f7813ee903a63f697ad390d3060121d0f45ee31031014c23a26f4287f3e6ad97d582131d0d568be31baa0b68f6f64ae337e926b3fe2625'
            'f435adba06f59406088306568d23f66d1947f9cc57e46cc929d5530e330ba3aa03b23773f98426a8b931732ee66731f3d3dad6885ad716691ef4992ba7613d43')

prepare() {
  cd libgudev-${pkgver}

  if [[ $pkgver = $_debver ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}


build() {
  cd libgudev-${pkgver}

  export CC='gcc -m32'
  export CXX='g++ -m32'
  export PKG_CONFIG_PATH='/usr/lib32/pkgconfig'

  ./configure \
    --prefix='/usr' \
    --libdir='/usr/lib32' \
    --localstatedir='/var' \
    --sysconfdir='/etc' \
    --disable-introspection \
    --disable-umockdev
  make
}

package() {
  cd libgudev-${pkgver}

  make DESTDIR="${pkgdir}" install
  rm -rf ${pkgdir}/usr/{include,share}
}

# vim: ts=2 sw=2 et:
