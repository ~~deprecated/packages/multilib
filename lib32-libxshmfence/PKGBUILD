# Maintainer (Arch): AndyRTR <andyrtr@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgbasename=libxshmfence
pkgname=lib32-$_pkgbasename
_openbsdver=6.5
pkgver=1.3
pkgrel=1.hyperbola1
pkgdesc="a library that exposes an event API on top of Linux kernel futexes (32-bit), provided by Xenocara"
arch=('x86_64')
url="https://www.xenocara.org"
license=('Expat')
depends=('lib32-glibc' "$_pkgbasename")
makedepends=('xenocara-util-macros' 'xenocara-proto' 'gcc-multilib')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxshmfence-$pkgver.tar.xz{,.sig}
        remove-forced-openbsd-futex-support.patch)
sha512sums=('09639d42c2f1c8853c386ed4b80817eba39ca06c4e38fbe18e9bb750450367c2d2379be26535a3f2e04dca6fb82ae339b168cb5fea9cbaa6522669a8136b2560'
            'SKIP'
            'f32ed726960475fba1c2c253f2f9d9e8d65bc562410c9db26c79f4645f70a32eeb1c5f5e6a0f270364cb1e79819cca99a9ffac6383e4c86fc298081442defc81')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  patch -p1 -i "$srcdir/remove-forced-openbsd-futex-support.patch"
  autoreconf -vfi
}

build() {
  export CC="gcc -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  cd "xenocara-$_openbsdver/lib/libxshmfence"
  ./configure --prefix=/usr --libdir=/usr/lib32
  make
}

check() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  make -k check
}

package() {
  cd "xenocara-$_openbsdver/lib/libxshmfence"
  make DESTDIR="$pkgdir" install

  rm -r "$pkgdir/usr/include"
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
