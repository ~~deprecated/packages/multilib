# Maintainer (Arch): Ionut Biru <ibiru@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgbasename=libxft
pkgname=lib32-$_pkgbasename
_openbsdver=6.5
pkgver=2.3.2
pkgrel=1.hyperbola1
pkgdesc="FreeType-based font drawing library for X (32-bit), provided by Xenocara"
arch=('x86_64')
license=('Expat')
url="https://www.xenocara.org"
depends=('lib32-fontconfig' 'lib32-libxrender' 'lib32-libbsd')
makedepends=('gcc-multilib' 'xenocara-util-macros')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxft-$pkgver.tar.xz{,.sig}
        add-libbsd-support.patch)
sha512sums=('c24759751b737f830b4808a48cda3b76cce57247283af4d54b0e6f2880fe6dd1d1d63153700d3de2f21e88e79ab3b8a8f33365effb3956d24995e0b50fdb0bfb'
            'SKIP'
            'd7a1909d318780337b8957f373a7ef2cb11edab9243f957e6ff544792d7e17a74d50bdb7fa0ad4d417d999ca5494bb2ee6015f5dd3755014845f07d2664dbaee')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libXft"
  patch -p1 -i "$srcdir/add-libbsd-support.patch"
  autoreconf -vfi
}

build() {
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"
  export LDFLAGS="$LDFLAGS -lbsd"

  cd "xenocara-$_openbsdver/lib/libXft"
  ./configure --prefix=/usr \
    --libdir=/usr/lib32 --disable-static
  make
}

package() {
  cd "xenocara-$_openbsdver/lib/libXft"
  make DESTDIR="$pkgdir" install

  rm -rf $pkgdir/usr/{bin,include,share}
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
