# Maintainer (Arch): Biru Ionut <ionut@archlinux.ro>
# Contributor (Arch): Mikko Seppälä <t-r-a-y@mbnet.fi>
# Contributor (Arch): Kaos < gianlucaatlas dot gmail dot com >
# Maintainer: André Silva <emulatorman@hyperbola.info>

_pkgbasename=sqlite
pkgname=lib32-sqlite
_srcver=3180000
_docver=${_srcver}
pkgver=3.18.0
_debver=3.16.2
_debrel=5+deb9u1
pkgrel=1.hyperbola1
pkgdesc="A C library that implements an SQL database engine (32-bit)"
arch=('x86_64')
license=('Public-Domain')
url="https://www.sqlite.org/index.html"
depends=(lib32-glibc $_pkgbasename)
makedepends=('tcl' 'gcc-multilib' 'lib32-readline' 'quilt')
source=(https://www.sqlite.org/2017/sqlite-src-${_srcver}.zip
        https://deb.debian.org/debian/pool/main/s/sqlite3/sqlite3_$_debver-$_debrel.debian.tar.xz
        license.txt
        CVE-2017-10989.patch)
sha512sums=('b82c5388066f2e905c067d75e890bf2ff03a522733b8c8e0ebdfffe1804d7ecb39b72f6d808fce59133ac8c645f8b6e163e3dff7bbb0d66bbe8748e069ca7a84'
            '9794ed5100f50bcf7a1d6bb0ae6b6a20543dda5b882ea046ff16e94bf0a00de70e79b56b88acc787514a0f5a76969acdf52694131a13a8015046d8416f3722a7'
            'a2e3294c5a8188a0c81d0cde13b6bc33a69b525cb101fb08e5d14ad7dce8342ddd170ef14306ae2adc5107f8b8774f0b20816e69294b6bed1d26473975b8a780'
            '93ef96e3b5d06bb02a0c279a599c25a0d0d139d09e84437df8d8c4fbdc11296851487bec655a85f3507990f610889d7526a8746d5b554ece60321e53e6db3c66')
options=('!makeflags')

prepare() {
  cd "$srcdir"/sqlite-src-$_srcver
  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    # Doesn't apply and seems unimportant
    rm -v debian/patches/02-use-packaged-lempar.c.patch || true
    rm -v debian/patches/20-hurd-locking-style.patch || true
    rm -v debian/patches/30-cross.patch || true

    quilt push -av
  fi
  patch -p1 -i ../CVE-2017-10989.patch
#  autoreconf -vfi
}

build() {
  cd "$srcdir"/sqlite-src-$_srcver

  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  export LTLINK_EXTRAS="-ldl"
  export CFLAGS="$CFLAGS -DSQLITE_ENABLE_FTS3=1 \
                         -DSQLITE_ENABLE_COLUMN_METADATA=1 \
                         -DSQLITE_ENABLE_UNLOCK_NOTIFY \
                         -DSQLITE_ENABLE_DBSTAT_VTAB=1 \
                         -DSQLITE_ENABLE_FTS3_TOKENIZER=1 \
                         -DSQLITE_ENABLE_RTREE=1 \
                         -DSQLITE_SECURE_DELETE \
                         -DSQLITE_MAX_VARIABLE_NUMBER=250000"

  ./configure --prefix=/usr \
    --libdir=/usr/lib32 \
    --disable-tcl \
    --disable-static

  make
}

package() {
  cd "$srcdir"/sqlite-src-$_srcver

  make DESTDIR=${pkgdir} install

  rm -rf "${pkgdir}"/usr/{include,share,bin}
  install -D -m644 "${srcdir}"/license.txt "${pkgdir}"/usr/share/licenses/${pkgname}/license.txt
}
