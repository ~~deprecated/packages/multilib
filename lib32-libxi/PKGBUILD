# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgbasename=libxi
pkgname=lib32-$_pkgbasename
_openbsdver=6.5
pkgver=1.7.9
pkgrel=2.hyperbola1
pkgdesc="X11 Input extension library (32-bit), provided by Xenocara"
arch=('x86_64')
url="https://www.xenocara.org"
depends=('lib32-libxext' 'lib32-libxfixes' 'xenocara-proto' "$_pkgbasename")
makedepends=('pkg-config' 'xenocara-util-macros' 'gcc-multilib' 'automake')
license=('X11')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxi-$pkgver.tar.xz{,.sig})
sha512sums=('8c475b20cf0cacb22c6c4fc73bd51107ae359d29d4c0e0919b2981bbbab6798f46801da9afdb1bc94f816eb346c999be0ae5a2788df065538d2950fbe6189d50'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libXi"
  autoreconf -vfi
}

build() {
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  cd "xenocara-$_openbsdver/lib/libXi"
  ./configure --prefix=/usr --sysconfdir=/etc --disable-static \
    --libdir=/usr/lib32

  make
}

package() {
  cd "xenocara-$_openbsdver/lib/libXi"
  make DESTDIR="$pkgdir" install

  rm -rf $pkgdir/usr/{include,share,bin}
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
