# Maintainer (Arch): Felix Yan <felixonmars@archlinux.org>
# Contributor (Arch): Jan Alexander Steffens (heftig) <jan.steffens@gmail.com>
# Contributor (Arch): Allan McRae <allan@archlinux.org>
# Contributor (Arch): Jochem Kossen <j.kossen@home.nl>
# Maintainer: André Silva <emulatorman@hyperbola.info>

_pkgbasename=fakeroot
pkgname=lib32-${_pkgbasename}
pkgver=1.21
_debver=1.21
_debrel=3.1
pkgrel=1.hyperbola2
pkgdesc='Tool for simulating superuser privileges (32-bit)'
arch=('x86_64')
license=('GPL-3')
url="https://packages.debian.org/fakeroot"
groups=('multilib-devel')
install=fakeroot.install
depends=('lib32-glibc' "$_pkgbasename")
makedepends=('po4a' 'quilt')
source=("https://deb.debian.org/debian/pool/main/f/${_pkgbasename}/${_pkgbasename}_${pkgver}.orig.tar.gz"
        "https://deb.debian.org/debian/pool/main/f/fakeroot/fakeroot_$_debver-$_debrel.debian.tar.xz")
sha512sums=('b55ec626bbe4ce7df7d3ea519db34961163ef824143d08366af2931e2576db6fa532e7cb49eab49f7566228058bc269555444d8bbc28b6937d1d43e9df11cb13'
            'befe26e18666f86c410ef2e326914e6ec4a2db65c0a725e4a025942fcaa69216dbd397e9d1ef5bb9648fc8defe9fb93334906402e60f6474111bbd904bd83da9')

prepare() {
  cd "${srcdir}/${_pkgbasename}-${pkgver}"

  if [[ ${pkgver%.*} = ${_debver%.*} ]]; then
    # Debian patches
    export QUILT_PATCHES=debian/patches
    export QUILT_REFRESH_ARGS='-p ab --no-timestamps --no-index'
    export QUILT_DIFF_ARGS='--no-timestamps'

    mv "$srcdir"/debian .

    quilt push -av
  fi
}

build() {
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  cd "${srcdir}/${_pkgbasename}-${pkgver}"
  ./bootstrap
  ./configure --prefix=/usr --libdir=/usr/lib32/libfakeroot \
              --disable-static --with-ipc=sysv
  make

  cd doc
  po4a -k 0 --rm-backups --variable "srcdir=../doc/" po4a/po4a.cfg
}

package() {
  cd "${srcdir}/${_pkgbasename}-${pkgver}"
  make DESTDIR=${pkgdir} install
  rm -rf "$pkgdir"/usr/{bin,share}

  install -dm755 ${pkgdir}/etc/ld.so.conf.d/
  echo '/usr/lib32/libfakeroot' > "$pkgdir/etc/ld.so.conf.d/$pkgname.conf"

  install -Dm644 COPYING "$pkgdir"/usr/share/licenses/$pkgname/COPYING
}
