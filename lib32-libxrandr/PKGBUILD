# Maintainer (Arch): Alexander Baldeck <alexander@archlinux.org>
# Contributor (Arch): Jan de Groot <jgc@archlinux.org>
# Maintainer: André Silva <emulatorman@hyperbola.info>
# Contributor: Márcio Silva <coadde@hyperbola.info>

_pkgbasename=libxrandr
pkgname=lib32-$_pkgbasename
_openbsdver=6.5
pkgver=1.5.1
pkgrel=1.hyperbola1
pkgdesc="X11 RandR extension library (32-bit), provided by Xenocara"
arch=('x86_64')
license=('Expat')
url="https://www.xenocara.org"
depends=('lib32-libxext' 'lib32-libxrender' "$_pkgbasename")
makedepends=('xenocara-util-macros' 'gcc-multilib')
source=(https://repo.hyperbola.info:50000/sources/xenocara-libre/$_openbsdver/lib/libxrandr-$pkgver.tar.xz{,.sig})
sha512sums=('cbaf4668aa5470b61eb1e039a2e23b7da329f3f73a2b0aa62794133da97e79540a0111eafe741a9f2d800887e365c505cd8bed7f355093475b9c7cc4933890e1'
            'SKIP')
validpgpkeys=('C92BAA713B8D53D3CAE63FC9E6974752F9704456') # André Silva

prepare() {
  cd "xenocara-$_openbsdver/lib/libXrandr"
  autoreconf -vfi
}

build() {
  export CC="gcc -m32"
  export CXX="g++ -m32"
  export PKG_CONFIG_PATH="/usr/lib32/pkgconfig"

  cd "xenocara-$_openbsdver/lib/libXrandr"
  ./configure --prefix=/usr --sysconfdir=/etc --libdir=/usr/lib32
  make
}

package() {
  cd "xenocara-$_openbsdver/lib/libXrandr"
  make DESTDIR="$pkgdir" install

  rm -rf $pkgdir/usr/{include,share,bin}
  install -Dm644 COPYING "$pkgdir/usr/share/licenses/$pkgname/COPYING"
}
